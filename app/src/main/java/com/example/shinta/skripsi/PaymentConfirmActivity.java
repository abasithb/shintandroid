package com.example.shinta.skripsi;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.shinta.skripsi.helper.RequestInterface;
import com.example.shinta.skripsi.model.OrderData;
import com.kosalgeek.android.photoutil.GalleryPhoto;
import com.kosalgeek.android.photoutil.ImageLoader;

import java.io.File;
import java.io.FileNotFoundException;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.example.shinta.skripsi.R.id.imageView;
import static com.example.shinta.skripsi.helper.RequestInterface.retrofit;

public class PaymentConfirmActivity extends AppCompatActivity {

    private static final int GALLERY_REQUEST = 678;
    private static final String TAG = "";

    @BindView(R.id.mTextOrderId)
    EditText orderid;
    @BindView(R.id.mButtonChoose)
    Button choose;
    @BindView(R.id.mButtonUpload)
    Button upload;

    private GalleryPhoto galleryPhoto;
    private String photoPath;
    private ProgressDialog mProgressDialog;
    private OrderData orderData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment_confirm);
        initLoading();
        ButterKnife.bind(this);

        orderData = (OrderData) getIntent().getSerializableExtra("data");

        orderid.setText(orderData.getId());
        Log.d(TAG, "onCreate() returned: " +orderData.getBank());
        Log.d(TAG, "onCreate() returned: " +orderData.getTotalPayment());
        Log.d(TAG, "onCreate() returned: " +orderData.getSender());
        Log.d(TAG, "onCreate() returned: " +orderData.getStatus());
        Log.d(TAG, "onCreate() returned: " +orderData.getUserId());

        galleryPhoto = new GalleryPhoto(this);

    }

    private void initLoading() {
        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setMessage("Loading...");
    }

    @OnClick(R.id.mButtonChoose)
    public void onChoose(View v){
        Intent in = galleryPhoto.openGalleryIntent();
        startActivityForResult(in, GALLERY_REQUEST);
    }

    @OnClick(R.id.mButtonUpload)
    public void onUpload(View v){
        doUpload();
    }

    private void doUpload() {
        mProgressDialog.show();
        final RequestInterface request = retrofit.create(RequestInterface.class);
        File file = new File(photoPath);
        String strOrder = orderid.getText().toString();
        RequestBody photo = RequestBody.create(MediaType.parse("image/*"), file);
        RequestBody order = RequestBody.create(MediaType.parse("text/plain"), strOrder);
        request.postUploads(photo, order).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    String res = response.body().string();

                    Log.d("UPLOAD RESPONSE", res);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                mProgressDialog.dismiss();

                if (response.isSuccessful()){
                    Toast.makeText(PaymentConfirmActivity.this, "Upload Berhasil", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                mProgressDialog.dismiss();
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode == RESULT_OK){
            if(requestCode == GALLERY_REQUEST){
                galleryPhoto.setPhotoUri(data.getData());
                photoPath = galleryPhoto.getPath();

            }
        }
    }
}
