package com.example.shinta.skripsi.helper;

import android.view.View;

/**
 * Created by shinta on 14/12/16.
 */

public interface OnClickListener {
    void onClick(View view, int position);
}
