package com.example.shinta.skripsi.helper;

import android.content.Context;
import android.content.SharedPreferences;

import com.example.shinta.skripsi.helper.Constant;

/**
 * Created by shinta on 20/11/16.
 */

public class SessionManager {

    private Context mContext;
    private SharedPreferences mPref;
    private SharedPreferences.Editor mEditor;

    public SessionManager(Context mContext) {
        this.mContext   = mContext;
        mPref           = mContext.getSharedPreferences(Constant.PREF_NAME, Context.MODE_PRIVATE);
        mEditor         = mPref.edit();
    }

    public void createSessionLogin(String token, String nama, String id) {
        mEditor.putBoolean(Constant.IS_LOGIN, true);
        mEditor.putString(Constant.TOKEN, token);
        mEditor.putString(Constant.NAMA, nama);
        mEditor.putString(Constant.ID, id);
        mEditor.commit();
    }

    public void getToken(){

    }

    public String getUserId(){
        return mPref.getString(Constant.ID, "1");
    }

    public void logout() {
        mEditor.clear().commit();
    }

    public boolean isLoggedOn() {
        return mPref.getBoolean(Constant.IS_LOGIN, false);
    }

}
