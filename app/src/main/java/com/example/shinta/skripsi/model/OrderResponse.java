package com.example.shinta.skripsi.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by shinta on 01/12/16.
 */

public class OrderResponse {

        @SerializedName("orders")
        @Expose
        private List<OrderData> orders = new ArrayList<OrderData>();

        /**
         *
         * @return
         * The orders
         */
        public List<OrderData> getOrders() {
            return orders;
        }

        /**
         *
         * @param orders
         * The orders
         */
        public void setOrders(List<OrderData> orders) {
            this.orders = orders;
        }

        @Override
        public String toString() {
                return "OrderResponse{" +
                        "orders=" + orders +
                        '}';
        }
}
