package com.example.shinta.skripsi.helper;

/**
 * Created by shinta on 20/11/16.
 */

public class Constant {

    //base url
    public static final String BASE_URL = "http://192.168.43.37:8000/api/";

    //endpoint
    public static final String POST_AUTH = "authenticate";
    public static final String POST_USER = "users";
    public static final String GET_CATALOGS = "catalogs";
    public static final String GET_ORDER = "orders";
    public static final String POST_ADDRESS = "address";
    public static final String GET_CATEGORIES = "categories";
    public static final String POST_UPLOADS = "uploads/input";

    //session manager
    public static final String PREF_NAME    = "ChaptaPref";
    public static final String TOKEN        = "token";
    public static final String IS_LOGIN     = "IS_LOGIN";
    public static final String NAMA         = "NAMA";
    public static final String ID         = "ID";
}
