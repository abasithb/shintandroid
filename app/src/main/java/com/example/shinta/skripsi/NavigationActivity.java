package com.example.shinta.skripsi;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.example.shinta.skripsi.helper.CatalogsAdapter;
import com.example.shinta.skripsi.helper.OnClickListener;
import com.example.shinta.skripsi.helper.RequestInterface;
import com.example.shinta.skripsi.helper.SessionManager;
import com.example.shinta.skripsi.model.CatalogsData;
import com.example.shinta.skripsi.model.CatalogsResponse;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.example.shinta.skripsi.helper.RequestInterface.retrofit;

public class NavigationActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, OnClickListener {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.drawer_layout)
    DrawerLayout drawer;
    @BindView(R.id.nav_view)
    NavigationView navigationView;
    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;


    private CatalogsAdapter adapter;
    private List<CatalogsData> data;
    private SessionManager mSessionManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_navigation);

        ButterKnife.bind(this);
        initView();

        data = new ArrayList<>();
        mSessionManager = new SessionManager(this);

//        Button buy = (Button) findViewById(R.id.mBtnBuy);
//
//        buy.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent buyproduct = new Intent(NavigationActivity.this, AddAddressActivity.class);
//                startActivity(buyproduct);
//            }
//        });

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.navigation, menu);
        return true;


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_menu) {

            Intent menu = new Intent(NavigationActivity.this, MyMenuActivity.class);
            startActivity(menu);
        } else if (id == R.id.action_cart) {
            // TODO: 11/12/16 cart activity
        } else if (id == R.id.action_logout) {
            mSessionManager.logout();
            finish();

            Intent intent = new Intent(this, LoginActivity.class);
            startActivity(intent);

        } else if(id == R.id.action_beli) {

            Intent intent = new Intent(this, AddAddressActivity.class);
            startActivity(intent);
        }


        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_beranda) {
            // Handle the camera action
        } else if (id == R.id.nav_promo) {

        } else if (id == R.id.nav_elektronik) {

        } else if (id == R.id.nav_fashion) {

        }

        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void initView() {

        setSupportActionBar(toolbar);

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        navigationView.setNavigationItemSelectedListener(this);

        recyclerView.setHasFixedSize(true);
//        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(this, 2);
        recyclerView.setLayoutManager(layoutManager);

    }


    @Override
    protected void onResume() {
        super.onResume();
        getCatalogList();
    }

    private void getCatalogList() {
        final RequestInterface request = retrofit.create(RequestInterface.class);
        request.getProducts().enqueue(new Callback<CatalogsResponse>() {
            @Override
            public void onResponse(Call<CatalogsResponse> call, Response<CatalogsResponse> response) {

                if (response.isSuccessful()) {
                    data.clear();
                    data = response.body().getProducts();
                    adapter = new CatalogsAdapter(NavigationActivity.this, data);
                    adapter.setOnClickListener(NavigationActivity.this);
                    recyclerView.setAdapter(adapter);
                }

            }

            @Override
            public void onFailure(Call<CatalogsResponse> call, Throwable t) {

                Toast.makeText(NavigationActivity.this, "gagal", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onClick(View view, int position) {
        switch (view.getId()){
            case R.id.mButtonBeli:
                Toast.makeText(this, "posisi "+data.get(position).getName(), Toast.LENGTH_SHORT).show();
                break;
        }
    }
}
