package com.example.shinta.skripsi;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.example.shinta.skripsi.helper.Constant;
import com.example.shinta.skripsi.helper.RequestInterface;
import com.example.shinta.skripsi.helper.SessionManager;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.io.IOException;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class LoginActivity extends AppCompatActivity {

    @BindView(R.id.edtLoginEmail)
    EditText edtLoginEmail;
    @BindView(R.id.edtLoginPassword)
    EditText edtLoginPassword;
    @BindString(R.string.error_field_required)
    String errorFieldRequired;

    private Intent intent = null;
    private ProgressDialog mProgressDialog;
    private SessionManager mSessionManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        initLoading();
        mSessionManager = new SessionManager(this);
        if (mSessionManager.isLoggedOn()) {
            intent = new Intent(LoginActivity.this, NavigationActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        }
        ButterKnife.bind(this);
    }

    private void initLoading() {
        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setMessage("Loading...");
    }

    @OnClick({R.id.btnlogin, R.id.btndaftar})
    public void click(View view) {

        switch (view.getId()) {
            case R.id.btnlogin:
                boolean cancel = validate();
                if (!cancel) {
                    doLogin(getInput());
                }
                break;
            case R.id.btndaftar:
                intent = new Intent(LoginActivity.this, RegisterActivity.class);
                break;
        }
        if (intent != null) {
            startActivity(intent);
        }
    }

    private JsonObject getInput() {
        JsonObject jsonInput = new JsonObject();
        jsonInput.addProperty("email", edtLoginEmail.getText().toString());
        jsonInput.addProperty("password", edtLoginPassword.getText().toString());
        return jsonInput;
    }

    private void doLogin(JsonObject input) {
        mProgressDialog.show();
        RequestInterface request = RequestInterface.retrofit.create(RequestInterface.class);
        request.login(input).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        String res = response.body().string();
                        JsonParser parser = new JsonParser();
                        JsonObject jsonResponse = parser.parse(res).getAsJsonObject();

                    mSessionManager.createSessionLogin(
                            jsonResponse.get(Constant.TOKEN).getAsString(),
                            "Shinta","1");
                    finish();

                    intent = new Intent(LoginActivity.this, NavigationActivity.class);
                        startActivity(intent);

                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else {
                    Toast.makeText(LoginActivity.this, "Gagal Login", Toast.LENGTH_SHORT).show();
                }
                mProgressDialog.dismiss();
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Toast.makeText(LoginActivity.this, t.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
                mProgressDialog.dismiss();
            }
        });
    }

    private boolean validate() {
        edtLoginEmail.setError(null);
        edtLoginPassword.setError(null);

        String email = edtLoginEmail.getText().toString();
        String passw = edtLoginPassword.getText().toString();

        boolean cancel = false;
        View focusView = null;

        if (TextUtils.isEmpty(passw)) {
            edtLoginPassword.setError(errorFieldRequired);
            focusView = edtLoginPassword;
            cancel = true;
        }

        if (TextUtils.isEmpty(email)) {
            edtLoginEmail.setError(errorFieldRequired);
            focusView = edtLoginEmail;
            cancel = true;
        }

        if (cancel)
            focusView.requestFocus();

        return cancel;
    }
}
