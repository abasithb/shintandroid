package com.example.shinta.skripsi;

import android.app.ProgressDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.shinta.skripsi.helper.RequestInterface;
import com.example.shinta.skripsi.model.RegisterData;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.example.shinta.skripsi.helper.RequestInterface.retrofit;


public class RegisterActivity extends AppCompatActivity implements View.OnClickListener {

    protected EditText mEditNama;
    protected EditText mEditEmail;
    protected EditText mEditPassword;
    protected Button button;
    protected RegisterData data;
    private ProgressDialog mProgressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        initLoading();

        data = new RegisterData();

        initView();
    }

    @Override
    public void onClick(View v) {

        if (v.getId() == R.id.button) {
            data.setName(mEditNama.getText().toString());
            data.setEmail(mEditEmail.getText().toString());
            data.setPassword(mEditPassword.getText().toString());
            postRegister(data);
        }

    }

    private void postRegister(RegisterData data){
        mProgressDialog.show();
        final RequestInterface request = retrofit.create(RequestInterface.class);
        request.postRegisterData(data.getName(), data.getEmail(), data.getPassword()).enqueue(new Callback<RegisterData>() {
            @Override
            public void onResponse(Call<RegisterData> call, Response<RegisterData> response) {
                if (response.isSuccessful()){

                    Toast.makeText(RegisterActivity.this, response.body().getName(), Toast.LENGTH_SHORT).show();

                }
            }

            @Override
            public void onFailure(Call<RegisterData> call, Throwable t) {
                Toast.makeText(RegisterActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                mProgressDialog.dismiss();

            }
        });
    }

    private void initLoading() {
        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setMessage("Loading...");
    }

    private void initView() {
        mEditNama = (EditText) findViewById(R.id.mEditNama);
        mEditEmail = (EditText) findViewById(R.id.mEditEmail);
        mEditPassword = (EditText) findViewById(R.id.mEditPassword);
        button = (Button) findViewById(R.id.button);
        button.setOnClickListener(RegisterActivity.this);
    }
}
