package com.example.shinta.skripsi;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.example.shinta.skripsi.R.id.button;

public class SelectBankActivity extends AppCompatActivity {

    @BindView(R.id.mBtnNext)
    Button next;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_bank);

        ButterKnife.bind(this);

    }

    @OnClick(R.id.mBtnNext)
    public void OnNext(View v){
        Intent btnnext = new Intent(SelectBankActivity.this, ReviewActivity.class);
        startActivity(btnnext);
    }



//    @Override
//    public void onClick(View v) {
//
//
//    }
}
