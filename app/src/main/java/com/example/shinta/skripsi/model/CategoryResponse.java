package com.example.shinta.skripsi.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by shinta on 14/12/16.
 */

public class CategoryResponse {

    @SerializedName("category")
    @Expose
    private List<CategoryData> category = null;

    /**
     *
     * @return
     * The category
     */
    public List<CategoryData> getCategory() {
        return category;
    }

    /**
     *
     * @param category
     * The category
     */
    public void setCategory(List<CategoryData> category) {
        this.category = category;
    }
}
