package com.example.shinta.skripsi.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by shinta on 30/11/16.
 */

public class RegisterResponse {

    @SerializedName("users")
    @Expose
    private List<RegisterData> users = new ArrayList<RegisterData>();

    /**
     *
     * @return
     * The users
     */
    public List<RegisterData> getUsers() {
        return users;
    }

    /**
     *
     * @param users
     * The users
     */
    public void setUsers(List<RegisterData> users) {
        this.users = users;
    }

}