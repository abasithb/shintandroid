package com.example.shinta.skripsi.helper;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.example.shinta.skripsi.R;
import com.example.shinta.skripsi.model.OrderData;

import java.util.List;

/**
 * Created by shinta on 29/11/16.
 */

public class OrderAdapter extends RecyclerView.Adapter<OrderAdapter.ViewHolder> {

    List<OrderData> orderDataList;
    OnClickListener listener;

    public OrderAdapter(List<OrderData> orderDataList) {
        this.orderDataList = orderDataList;
    }

    public void setOnClickListener(OnClickListener listener) {
        this.listener = listener;
    }

    @Override
    public OrderAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_order_check_card, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final OrderAdapter.ViewHolder holder, int position) {
        holder.tv_tgl.setText(String.valueOf("Waktu : " + orderDataList.get(position).getCreatedAt()));
        holder.tv_total.setText(String.valueOf("Total : Rp." + orderDataList.get(position).getTotalPayment()));
        holder.tv_status.setText(String.valueOf("Status :" + orderDataList.get(position).getStatus()));
        holder.buttonConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onClick(v, holder.getAdapterPosition());
            }
        });
    }

    @Override
    public int getItemCount() {
        return orderDataList.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {

        private TextView tv_tgl, tv_total, tv_status;
        private Button buttonConfirm;

        public ViewHolder(View itemView) {
            super(itemView);
            tv_tgl = (TextView) itemView.findViewById(R.id.mTextTgl);
            tv_total = (TextView) itemView.findViewById(R.id.mTextTotal);
            tv_status = (TextView) itemView.findViewById(R.id.mTextStatus);
            buttonConfirm = (Button) itemView.findViewById(R.id.mBtnConfirm);
        }
    }
}
