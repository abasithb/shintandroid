package com.example.shinta.skripsi.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by shinta on 30/11/16.
 */


public class CatalogsResponse {

    @SerializedName("products")
    @Expose
    private List<CatalogsData> products = new ArrayList<CatalogsData>();

    /**
     *
     * @return
     * The products
     */
    public List<CatalogsData> getProducts() {
        return products;
    }

    /**
     *
     * @param products
     * The products
     */
    public void setProducts(List<CatalogsData> products) {
        this.products = products;
    }

}