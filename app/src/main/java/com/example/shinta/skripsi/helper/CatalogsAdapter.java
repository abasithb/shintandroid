package com.example.shinta.skripsi.helper;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.shinta.skripsi.R;
import com.example.shinta.skripsi.model.CatalogsData;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.example.shinta.skripsi.R.id.imageView;

/**
 * Created by shinta on 30/11/16.
 */

public class CatalogsAdapter extends RecyclerView.Adapter<CatalogsAdapter.ViewHolder> {

    List<CatalogsData> catalogsData;
    Context context;
    OnClickListener listener;

    public void setOnClickListener ( OnClickListener listener){
        this.listener = listener;
    }

    public CatalogsAdapter(Context context, List<CatalogsData> catalogsData) {
        this.context = context;
        this.catalogsData = catalogsData;
    }

    @Override
    public CatalogsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_catalogs_card, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final CatalogsAdapter.ViewHolder holder, int position) {
        CatalogsData catalogsData = getCatalogsData(position);
        holder.name.setText(catalogsData.getName());
        holder.price.setText(String.valueOf("Rp." + catalogsData.getPrice()));
        Picasso.with(context).load(catalogsData.getPhotoPath()).into(holder.image);
        holder.beli.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onClick(v,holder.getAdapterPosition());
            }
        });
    }

    private CatalogsData getCatalogsData(int position) {
        return catalogsData.get(position);
    }

    @Override
    public int getItemCount() {
        return catalogsData.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.mTextNama)
        TextView name;

        @BindView(R.id.mTextHarga)
        TextView price;

        @BindView(R.id.mImageProduct)
        ImageView image;

        @BindView(R.id.mButtonBeli)
        Button beli;


        public ViewHolder(View itemView) {
            super(itemView);

            ButterKnife.bind(this, itemView);

        }
    }
}


