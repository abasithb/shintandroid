package com.example.shinta.skripsi;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.shinta.skripsi.helper.RequestInterface;
import com.example.shinta.skripsi.model.AddressData;

import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.example.shinta.skripsi.R.id.button;
import static com.example.shinta.skripsi.helper.RequestInterface.retrofit;


public class AddAddressActivity extends AppCompatActivity implements View.OnClickListener {

    protected EditText mEditNama;
    protected EditText mEditAlamat;
    protected EditText mEditTelpon;
    protected Button button;
    protected AddressData data;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_address);

        data = new AddressData();

        initView();
    }

    private void initView() {

        mEditNama = (EditText) findViewById(R.id.mTextNama);
        mEditAlamat = (EditText) findViewById(R.id.mTextAlamat);
        mEditTelpon = (EditText) findViewById(R.id.mTextTlp);
        button = (Button) findViewById(R.id.mBtnProses);
        button.setOnClickListener(AddAddressActivity.this);
    }


    @Override
    public void onClick(View v) {
       if (v.getId() == R.id.mBtnProses) {
//            data.setName(mEditNama.getText().toString());
//            data.setDetail(mEditAlamat.getText().toString());
//            data.setPhone(mEditTelpon.getText().toString());
//            postAddress(data);

           Intent addaddress = new Intent(this,SelectBankActivity.class);
           startActivity(addaddress);

       }

        }

    }

//    private void postAddress(AddressData data) {
//        final RequestInterface request = retrofit.create(RequestInterface.class);
//        request.postAddressData(data.getName(), data.getDetail(), data.getPhone()).enqueue(new Callback<AddressData>() {
//            @Override
//            public void onResponse(Call<AddressData> call, Response<AddressData> response) {
//
//            }
//
//            @Override
//            public void onFailure(Call<AddressData> call, Throwable t) {
//
//            }
//        });
//    }
//}
