package com.example.shinta.skripsi;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;

import com.example.shinta.skripsi.helper.OnClickListener;
import com.example.shinta.skripsi.helper.OrderAdapter;
import com.example.shinta.skripsi.helper.RequestInterface;
import com.example.shinta.skripsi.model.OrderData;
import com.example.shinta.skripsi.model.OrderResponse;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.example.shinta.skripsi.helper.RequestInterface.retrofit;

public class OrderCheckActivity extends AppCompatActivity implements OnClickListener {

    protected RecyclerView recyclerView;
    protected OrderAdapter adapter;
    protected List<OrderData> data;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_check);
        data = new ArrayList<>();
        initView();
    }

    @Override
    protected void onResume() {
        super.onResume();
        getOrdertList();
    }

    private void getOrdertList() {

        final RequestInterface request = retrofit.create(RequestInterface.class);
        request.getOrders().enqueue(new Callback<OrderResponse>() {
            @Override
            public void onResponse(Call<OrderResponse> call, Response<OrderResponse> response) {
                Log.d("afasfa", "onResponse() returned: " + response.body().toString());
                data.clear();
                data = response.body().getOrders();
                adapter = new OrderAdapter(data);
                adapter.setOnClickListener(OrderCheckActivity.this);
                recyclerView.setAdapter(adapter);
            }

            @Override
            public void onFailure(Call<OrderResponse> call, Throwable t) {

            }
        });
    }


    private void initView() {

        recyclerView = (RecyclerView) findViewById(R.id.recycler_view_order);
        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager((getApplicationContext()));
        recyclerView.setLayoutManager(layoutManager);
    }

    @Override
    public void onClick(View view, int position) {
        switch (view.getId()){
            case R.id.mBtnConfirm:
                Intent intent = new Intent(this, PaymentConfirmActivity.class);
                intent.putExtra("data", data.get(position));
                startActivity(intent);
                break;
        }
    }
}
