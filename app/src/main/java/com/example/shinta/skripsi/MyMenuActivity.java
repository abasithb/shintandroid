package com.example.shinta.skripsi;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;

public class MyMenuActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_menu);

       /* ImageButton btncek1 = (ImageButton) findViewById(R.id.mBtnCheck1);

        btncek1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent cekpesan = new Intent(MyMenuActivity.this,OrderCheckActivity.class);
                startActivity(cekpesan);
            }
        });*/

//        Button btncek3 = (Button) findViewById(R.id.mBtnCheck3);

        Button payment = (Button) findViewById(R.id.mBtnPayment);

        payment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent confirmpayment = new Intent(MyMenuActivity.this, PaymentConfirmActivity.class);
                startActivity(confirmpayment);
            }
        });

        Button order = (Button) findViewById(R.id.mBtnCheck);

        order.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent ordercheck = new Intent(MyMenuActivity.this, OrderCheckActivity.class);
                startActivity(ordercheck);
            }
        });
    }
}
