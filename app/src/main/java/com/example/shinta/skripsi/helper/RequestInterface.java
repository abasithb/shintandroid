package com.example.shinta.skripsi.helper;

import android.icu.util.ULocale;

import com.example.shinta.skripsi.model.AddressData;
import com.example.shinta.skripsi.model.CatalogsData;
import com.example.shinta.skripsi.model.CatalogsResponse;
import com.example.shinta.skripsi.model.CategoryResponse;
import com.example.shinta.skripsi.model.OrderResponse;
import com.example.shinta.skripsi.model.RegisterData;
import com.google.gson.JsonObject;

import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

/**
 * Created by shinta on 20/11/16.
 */

public interface RequestInterface {

    Retrofit retrofit = new Retrofit.Builder()
            .baseUrl(Constant.BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build();

    @POST(Constant.POST_AUTH)
    Call<ResponseBody> login(@Body JsonObject jsonBody);

//    @POST(Constant.POST_USER)
//    Call<ResponseBody> register(@Body JsonObject jsonBody);

    @FormUrlEncoded
    @POST(Constant.POST_USER)
    Call<RegisterData> postRegisterData (
            @Field("name") String name,
            @Field("email") String email,
            @Field("password") String password
    );
    

    @GET(Constant.GET_CATALOGS)
    Call<CatalogsResponse> getProducts();
    
    
    //// TODO: 14/12/16
    @GET(Constant.GET_CATEGORIES)
    Call<CategoryResponse> getCategory();

    @GET(Constant.GET_ORDER)
    Call<OrderResponse> getOrders();

    @FormUrlEncoded
    @POST(Constant.POST_ADDRESS)
    Call<AddressData> postAddressData (
            @Field("name") String name,
            @Field("detail") String detail,
            @Field("phone") String phone
    );

    @Multipart
    @POST(Constant.POST_UPLOADS)
    Call<ResponseBody> postUploads(
            @Part("photo") RequestBody photo,
            @Part("order_id") RequestBody orderid);


}
