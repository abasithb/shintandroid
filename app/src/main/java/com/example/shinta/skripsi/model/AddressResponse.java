package com.example.shinta.skripsi.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by shinta on 02/12/16.
 */

public class AddressResponse {

    @SerializedName("category")
    @Expose
    private List<AddressData> category = new ArrayList<AddressData>();

    /**
     *
     * @return
     * The category
     */
    public List<AddressData> getCategory() {
        return category;
    }

    /**
     *
     * @param category
     * The category
     */
    public void setCategory(List<AddressData> category) {
        this.category = category;
    }

}
